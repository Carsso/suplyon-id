<?php

class sfApplyResetForm extends sfForm
{
  public function configure()
  {
    $this->setWidget( 'password', 
            new sfWidgetFormInputPassword( array(), array('maxlength' => 128, 'title'=>'NOUVEAU Mot de passe', 'class'=>'designedinput')));
    $this->setWidget('password2', 
            new sfWidgetFormInputPassword( array(), array('maxlength' => 128, 'title'=>'Confirmation du NOUVEAU mot de passe', 'class'=>'designedinput')));
    
    $this->widgetSchema->setNameFormat('sfApplyReset[%s]');
    //$this->widgetSchema->setFormFormatterName('list');

        $this->setValidator( 'password', new sfValidatorString(array('required' => true,'trim' => true,'min_length' => 6,'max_length' => 128), array('min_length'=>'Trop court')));
        $this->setValidator( 'password2', new sfValidatorString(array('required' => true,'trim' => true,'min_length' => 6,'max_length' => 128), array('min_length'=>'Trop court')));

        $schema = $this->validatorSchema;

        // Hey Fabien, adding more postvalidators is kinda verbose!
        $postValidator = $schema->getPostValidator();
        
        $postValidators = array( 
            new sfValidatorSchemaCompare( 'password', sfValidatorSchemaCompare::EQUAL,
                    'password2', array(), array('invalid' => 'Les mots de passe ne correspondent pas.')));

        if( $postValidator )
        {
            $postValidators[] = $postValidator;
        }


        $this->validatorSchema->setPostValidator( new sfValidatorAnd($postValidators) );
    
  }
  public function getStylesheets()
  {
    return array( '/sfForkedDoctrineApplyPlugin/css/forked' => 'all' );
  }
}

