<?php use_helper('I18N') ?>
<?php slot('sf_apply_login') ?>
<?php end_slot() ?>
<div class="sf_apply sf_apply_reset">
<form method="post" action="<?php echo url_for("sfApply/reset") ?>" name="sf_apply_reset_form" id="sf_apply_reset_form">
<p>
<?php echo __('Thanks for confirming your email address. You may now change your
password using the form below.', array(), 'sfForkedApply') ?>
</p>
<?php echo $form['password']->getError() ?>
<p>
  <label class="hidden" for="sfApplyReset_password">NOUVEAU Mot de passe</label> <?php echo $form['password'] ?>
</p>
<?php echo $form['password2']->renderError() ?>
<p>
  <label class="hidden" for="sfApplyReset_password2">Confirmer le NOUVEAU Mot de passe</label> <?php echo $form['password2'] ?>
</p>
<p>
  <?php echo $form->renderHiddenFields(); ?>
  <input type="submit" value="<?php echo __("Reset My Password", array(), 'sfForkedApply') ?>" class="designedbutton">
</p>
</form>
</div>
