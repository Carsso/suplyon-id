
<?php use_helper("I18N") ?>
<div class="sf_apply sf_apply_settings">
<h2><?php echo __("Account Settings", array(), 'sfForkedApply') ?></h2>
<form method="post" action="<?php echo url_for("sfApply/settings") ?>" name="sf_apply_settings_form" id="sf_apply_settings_form">
  <p>
    <?php echo $form['firstname']->renderError() ?>
    <label class="hidden" for="sfApplySettings_firstname">Prénom</label> <?php echo $form['firstname'] ?>
  </p>
  <p>
    <?php echo $form['lastname']->renderError() ?>
    <label class="hidden" for="sfApplySettings_firstname">Nom</label> <?php echo $form['lastname'] ?>
  </p>
  <p>
    <?php echo $form->renderHiddenFields(); ?>
    <input type="submit" value="<?php echo __("Save", array(), 'sfForkedApply') ?>"  class="designedbutton" />
  </p>
 </form>
<form method="get" action="<?php echo url_for("sfApply/resetRequest") ?>" name="sf_apply_reset_request" id="sf_apply_reset_request">
<p>
<?php echo __('Click the button below to change your password.', array(), 'sfForkedApply'); ?>
<?php
$confirmation = sfConfig::get( 'app_sfForkedApply_confirmation' );
if( $confirmation['reset_logged'] ): ?>
    <?php echo __('For security reasons, you
will receive a confirmation email containing a link allowing you to complete the password change.', array(), 'sfForkedApply') ?>
<?php endif; ?>
</p>
<input type="submit" value="<?php echo __("Reset Password", array(), 'sfForkedApply') ?>"  class="designedbutton" />
</form>
</div>
