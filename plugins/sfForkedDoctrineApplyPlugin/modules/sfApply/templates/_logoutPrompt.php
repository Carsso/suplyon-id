<?php use_helper('I18N') ?>
<div id="sf_apply_logged_in_as">
<h4>
<?php echo __("Logged in as %1%", 
  array("%1%" => '</h4><h5><b>'.$sf_user->getGuardUser()->getFirstName().' '.$sf_user->getGuardUser()->getLastName().'</b> ('.$sf_user->getGuardUser()->getEmailAddress().')'),'sfForkedApply') ?>
</h5>
<p>
    <a href="<?php echo url_for('sfApply/settings') ?>" title="Paramètres">Paramètres</a>
</p>
<p>
<?php echo link_to(__('Log Out', array(), 'sfForkedApply'),
  '@sf_guard_signout', array("id" => 'logout','class'=>'designedbutton')) ?>
</p>
</div>

