<?php use_helper('I18N') ?>
<?php use_stylesheets_for_form( $form ) ?>
<?php slot('sf_apply_login') ?>
<?php end_slot() ?>
<div class="sf_apply sf_apply_apply estateitem bordered shadowed rounded text_center">
<h4>Mot de passe oublié</h4>
<form method="POST" action="<?php echo url_for('sfApply/resetRequest') ?>"
  name="sf_apply_reset_request" id="sf_apply_reset_request">
<p>
<?php echo __('Forgot your username or password? No problem! Just enter your username <strong>or</strong>
your email address and click "Reset My Password." You will receive an email message containing both your username and
a link permitting you to change your password if you wish.', array(), 'sfForkedApply') ?>
</p>
<?php echo $form['username_or_email']->renderError() ?>
<p>
  <label class="hidden" for="signin_username">Adresse Email</label> <?php echo $form['username_or_email'] ?>
</p>

  <?php echo $form['captcha']->renderError() ?>
<?php echo $form['captcha'] ?>
<p>
  <?php echo $form->renderHiddenFields(); ?>
<input type="submit" class="designedbutton" value="<?php echo __("Reset My Password", array(), 'sfForkedApply') ?>">
</p>
</ul>
</form>
</div>
