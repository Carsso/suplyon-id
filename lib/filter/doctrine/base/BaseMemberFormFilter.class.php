<?php

/**
 * Member filter form base class.
 *
 * @package    SupLyon
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedInheritanceTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseMemberFormFilter extends sfGuardUserProfileFormFilter
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('member_filters[%s]');
  }

  public function getModelName()
  {
    return 'Member';
  }
}
