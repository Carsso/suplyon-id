<?php

/**
 * Site filter form base class.
 *
 * @package    SupLyon
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseSiteFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'image'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'domain'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'informations' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'credentials'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'privatekey'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'name'         => new sfValidatorPass(array('required' => false)),
      'image'        => new sfValidatorPass(array('required' => false)),
      'domain'       => new sfValidatorPass(array('required' => false)),
      'informations' => new sfValidatorPass(array('required' => false)),
      'credentials'  => new sfValidatorPass(array('required' => false)),
      'privatekey'   => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('site_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Site';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'name'         => 'Text',
      'image'        => 'Text',
      'domain'       => 'Text',
      'informations' => 'Text',
      'credentials'  => 'Text',
      'privatekey'   => 'Text',
    );
  }
}
