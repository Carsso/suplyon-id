<?php

/**
 * sfGuardUserProfile filter form base class.
 *
 * @package    SupLyon
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BasesfGuardUserProfileFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'user_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('User'), 'add_empty' => true)),
      'type'           => new sfWidgetFormFilterInput(),
      'pseudo'         => new sfWidgetFormFilterInput(),
      'name'           => new sfWidgetFormFilterInput(),
      'role'           => new sfWidgetFormFilterInput(),
      'campusid'       => new sfWidgetFormFilterInput(),
      'campus'         => new sfWidgetFormFilterInput(),
      'class'          => new sfWidgetFormFilterInput(),
      'cursus'         => new sfWidgetFormFilterInput(),
      'promotion'      => new sfWidgetFormFilterInput(),
      'rank'           => new sfWidgetFormFilterInput(),
      'teacher'        => new sfWidgetFormFilterInput(),
      'fullprof'       => new sfWidgetFormFilterInput(),
      'last_connexion' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'created_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'slug'           => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'user_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('User'), 'column' => 'id')),
      'type'           => new sfValidatorPass(array('required' => false)),
      'pseudo'         => new sfValidatorPass(array('required' => false)),
      'name'           => new sfValidatorPass(array('required' => false)),
      'role'           => new sfValidatorPass(array('required' => false)),
      'campusid'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'campus'         => new sfValidatorPass(array('required' => false)),
      'class'          => new sfValidatorPass(array('required' => false)),
      'cursus'         => new sfValidatorPass(array('required' => false)),
      'promotion'      => new sfValidatorPass(array('required' => false)),
      'rank'           => new sfValidatorPass(array('required' => false)),
      'teacher'        => new sfValidatorPass(array('required' => false)),
      'fullprof'       => new sfValidatorPass(array('required' => false)),
      'last_connexion' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'slug'           => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('sf_guard_user_profile_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'sfGuardUserProfile';
  }

  public function getFields()
  {
    return array(
      'user_id'        => 'ForeignKey',
      'type'           => 'Text',
      'idbooster'      => 'Number',
      'pseudo'         => 'Text',
      'name'           => 'Text',
      'role'           => 'Text',
      'campusid'       => 'Number',
      'campus'         => 'Text',
      'class'          => 'Text',
      'cursus'         => 'Text',
      'promotion'      => 'Text',
      'rank'           => 'Text',
      'teacher'        => 'Text',
      'fullprof'       => 'Text',
      'last_connexion' => 'Date',
      'created_at'     => 'Date',
      'updated_at'     => 'Date',
      'slug'           => 'Text',
    );
  }
}
