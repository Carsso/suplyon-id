<div class="connexion right_bloc connexion_bloc greycolor">
    <h2 class="header_right_bloc">Espace d'authentification centralisée</h2>
    <div class="article">
        <div class="article_title"><h3>Accès interdit</h3></div>
        <p>
            Vous n'avez pas les droits nécessaires pour accèder à cette page.
        </p>
        <p>
          <?php echo $error_secure_msg ?>
        </p>
    </div>
</div>