
<?php if($sf_user->isAuthenticated()): ?>

<h2 class="header_right_bloc"><?php echo $sf_user->getProfile()->getName() ?></h2>
<form action="<?php echo url_for('@sf_guard_signout') ?>" id="connexion_form" method="post">
    <div id="userphoto">
      <?php echo image_tag(url_for('student_big_photo',$sf_user->getProfile()).'?force=true', array('style'=>'width:215px;', 'title'=>$sf_user->getProfile()->getName())) ?>
    </div>
    <input name="connection" type="submit" value="Déconnexion" />
</form>
<?php if($sf_user->getGuardUser()->getIsSuperAdmin() && !$sf_user->getAttribute('fakelogin',null)): ?>
    <br />
    <form action="<?php echo url_for('@auth_fakelogin?idbooster=') ?>" id="fakelogin_form" method="post">
        <input name="idbooster" id="campusid" type="text" placeholder="Fake Campus ID" /><br />
        <input name="connection" type="submit" value="Usurper" />
    </form>


    <script type="text/javascript">
        $('#fakelogin_form').submit(function(){
            if($('#campusid').val()==''){
                return false;
            }
            $('#fakelogin_form').attr('action',$('#fakelogin_form').attr('action')+$('#campusid').val());
        });
    </script>
  <?php endif; ?>
<?php else: ?>

<h2 class="header_right_bloc">Espace d'authentification centralisée</h2>
<form action="<?php echo url_for('@auth_openid') ?>" id="connexion_form" method="post">
    <input name="idbooster" id="campusid" type="text" placeholder="Campus ID" /><br />
    <input name="connection" type="submit" value="Se connecter" />
</form>

<script type="text/javascript">
    $('#connexion_form').submit(function(){
        if($('#campusid').val()==''){
            return false;
        }
    });
</script>
<?php endif; ?>