<div class="connexion right_bloc connexion_bloc greycolor">
    <h2 class="header_right_bloc">Espace d'authentification centralisée</h2>
    <form action="<?php echo $redir_url ?>" id="connexion_form" method="post">
        <input name="idbooster" id="idbooster" type="hidden" value="<?php echo $redir_idbooster ?>" />
        <input name="token" id="token" type="hidden" value="<?php echo $redir_token ?>" />
        <input type="submit" value="Redirection" />
    </form>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#connexion_form').submit();
        });
    </script>
    <div class="clear"></div>
</div>