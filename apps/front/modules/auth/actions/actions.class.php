<?php
class authActions extends BasesfPHPOpenIDAuthActions
{
    public function executeOpenidError()
    {
        $this->error = $this->getUser()->getFlash('openid_error');
        amgSentry::sendMessage($this->error);
        $this->redirect('@homepage');
    }

    public function openIDCallback($openid_validation_result)
    {
        $this->getUser()->setAuthenticated(true);
        $userdata = array();
        foreach ($openid_validation_result['userData'] as $datakey => $data) {
            $userdata[$datakey] = (is_array($data)) ? implode(';', $data) : $data;
            while (is_array($userdata[$datakey])) {
                $userdata[$datakey] = implode(';', $userdata[$datakey]);
            }
        }
        $user = Doctrine::getTable('sfGuardUserProfile')->findOneBy('idbooster', $userdata['idbooster']);
        if (!is_object($user) OR !is_object($user->getUser())) {
            $this->getResponse()->setCookie('idbooster', $userdata['idbooster']);
            $guarduser = new sfGuardUser();
            $guarduser->setEmailAddress($userdata['idbooster'] . '@supinfo.com')
                ->setUsername($userdata['fullname'])
                ->setEmailAddress($userdata['idbooster'] . '@supinfo.com')
                ->setIsActive(1)
                ->save();
            $user = new Member();
            $user->setUserId($guarduser->getId())
                ->setPseudo($userdata['idbooster']);
        } else {
            $guarduser = $user->getUser();
        }
        $user->setName($userdata['fullname'])
            ->setIdbooster($userdata['idbooster'])
            ->setCampusid($userdata['campusid'])
            ->setCampus(html_entity_decode($userdata['campus'], ENT_COMPAT, 'UTF-8'))
            ->setClass($userdata['class'])
            ->setPromotion($userdata['promotion'])
            ->setCursus($userdata['cursus'] . ' ' . $userdata['cursusdetail'])
            ->setRole($userdata['role'])
            ->setRank($userdata['rank'])
            ->setTeacher($userdata['teachersubjects'])
            ->setFullprof($userdata['fullprofsubjects'])
            ->save();
        $this->getResponse()->setCookie('idbooster', $userdata['idbooster']);
        $this->getUser()->signIn($guarduser, true);
        $this->redirect('@redir');
    }

    public function executeIndex(sfWebRequest $request)
    {
    }

    public function executeLogin(sfWebRequest $request)
    {
        $this->setTemplate('index');
    }

    public function executeInfos(sfWebRequest $request)
    {
        $site = Doctrine_Core::getTable('Site')->findOneBy('privatekey', $request->getParameter('site_key'));
        $member_site = Doctrine_Core::getTable('MemberSite')->findOneBy('privatekey', $request->getParameter('member_site_key'));
        if ($site && $member_site) {
            if ($member_site->getSite() == $site) {
                $user = $member_site->getMember();
                return $this->renderText($member_site->getDatasent());
            } else {
                return $this->renderText(json_encode(array('error' => 'Bad Site')));
            }
        } else {
            return $this->renderText(json_encode(array('error' => 'Bad Key')));
        }
    }

    public function executeRedir(sfWebRequest $request)
    {
        $redir = $this->getUser()->getAttribute('redir');
        $redir_final = $this->getUser()->getAttribute('redir_final');
        if ($redir_final) {
            $redir = $redir_final;
        }
        if ($redir) {
            $url_parsed = parse_url($redir);
            $site = Doctrine_Core::getTable('Site')->findOneBy('domain', $url_parsed['host']);
            if ($site) {
                do {
                    $private_key = md5(uniqid());
                } while (Doctrine_Core::getTable('Site')->findOneBy('privatekey', $private_key));
                $profile = $this->getUser()->getProfile();
                $guard_user = $this->getUser()->getGuardUser();
                $profile_array = array_merge($profile->toArray(), $guard_user->toArray());
                $credentials = explode(',', $site->getCredentials());
                $default_rights = array('idbooster', 'name', 'email_address');
                $credentials = array_merge($credentials, $default_rights);
                $datasent = array();
                foreach ($credentials as $credential) {
                    $credential = trim($credential);
                    if (isset($profile_array[$credential])) {
                        $datasent[$credential] = $profile_array[$credential];
                    } else {
                        $datasent[$credential] = null;
                    }
                }
                $membersite = new MemberSite();
                $membersite->setMember($profile)
                    ->setSite($site)
                    ->setDatasent(json_encode($datasent))
                    ->setPrivatekey($private_key)
                    ->save();
                $this->redir_url = $redir;
                $this->redir_idbooster = $this->getUser()->getProfile()->getIdbooster();
                $this->redir_token = $private_key;
                $this->getUser()->setAttribute('redir', false);
                $this->getUser()->setAttribute('redir_final', false);
            } else {
                $this->redirect('@homepage');
            }
        } else {
            $this->redirect('@homepage');
        }

    }

    public function executeLoginOpenId(sfWebRequest $request)
    {
        if ($request->getReferer()) {
            $this->getUser()->setAttribute('redir', $request->getReferer());
        }

        if ($request->getParameter('idbooster')) {
            $idbooster = $request->getParameter('idbooster');
            $getRedirectHtmlResult = $this->getRedirectHtml('https://id.supinfo.com/me/' . $idbooster);
            $debug = 'Full Login';
        } elseif ($this->getUser()->isAuthenticated()) {
            $idbooster = $this->getUser()->getProfile()->getIdbooster();
            $getRedirectHtmlResult = $this->getRedirectHtml('https://id.supinfo.com/me/' . $idbooster);
            $debug = 'Re-Login';
        } elseif ($request->getReferer()) {
            $this->getUser()->setAttribute('redir_final', $request->getReferer());
            $this->redirect('@login');
            $debug = 'Final-Login';
        }

        if ($this->getUser()->isAuthenticated()) {
            if ($this->getUser()->getProfile()->getIdbooster() == $request->getParameter('idbooster')) {
                $this->redirect('@redir');
                $debug = 'User-Login-Ok';
            } else {
                $redir_bkp = $this->getUser()->getAttribute('redir');
                $this->getUser()->signOut();
                $this->getUser()->setAttribute('redir', $redir_bkp);
                $debug = 'User-Login-No';
            }
        }

        if (!empty($getRedirectHtmlResult['url']) && !$request->isXmlHttpRequest()) {
            $this->redirect($getRedirectHtmlResult['url']);
        }
        $this->forward404('[Erreur OpenID] ' . $debug . ' '.var_export($getRedirectHtmlResult, true).' !');

    }

    public function executeFakeLogin(sfWebRequest $request)
    {
        if ($user = Doctrine::getTable('sfGuardUserProfile')->findOneBy('idbooster', $request->getParameter('idbooster'))) {
            $guarduser = $user->getUser();
            if (!$this->getUser()->getAttribute('fakelogin', null)) {
                if ($this->getUser()->isAuthenticated() && $this->getUser()->getGuardUser()->getIsSuperAdmin()) {
                    amgSentry::sendMessage('[Usurpation][En cours] Usurpation de l\'identité de ' . $guarduser . ' par ' . $this->getUser()->getProfile() . ' en cours...');
                    $this->getUser()->signIn($guarduser, true);
                    $this->getUser()->setAttribute('fakelogin', $this->getUser()->getProfile());
                    $back = '@homepage';
                    $this->redirect($back);
                } else {
                    $this->forward404('[Usurpation][Non autorisé] Accès autorisé uniquement avec compte admin logué !');
                }
            } else {
                $this->forward404('[Usurpation][Déjà en cours] Usurpation déjà en cours !');
            }
        } else {
            $this->forward404('[Usurpation][Inexistant] Utilisateur "' . $request->getParameter('idbooster') . '" inexistant.');
        }
    }

    public function executeSecure(sfWebRequest $request)
    {
        if ($this->getRequest()->hasParameter('error_secure_msg')) {
            $this->error_secure_msg = $this->getRequest()->getParameter('error_secure_msg', null);
        }
    }
}
