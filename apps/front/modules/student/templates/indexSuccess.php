<div class="connexion right_bloc connexion_bloc greycolor">
    <h2 class="header_right_bloc">Espace d'authentification centralisée</h2>
    <?php
      echo '<div class="article"><div class="article_title"><h3>SUPINFO International University</h3></div><div class="article_content">';
      $prec_campusid=0;
      $prec_class='0';
      foreach($students as $student){
        if($student->getCampusid() != $prec_campusid){
          $prec_class='0';
          $prec_campusid=$student->getCampusid();
          echo '<div class="clear"></div></div></div>';
          echo '<div class="article"><div class="article_title"><h3>'.$student->getCampus().'</h3></div><div class="article_content">';
        }
        if($student->getClass() != $prec_class){
          $prec_class=$student->getClass();
          $class_todisplay=$student->getClass();
          if($prec_class==''){
            $class_todisplay='N/A';
          }
          echo '<div class="clear"></div>';
          echo '<div class="std_class" style="margin:10px;">'.$class_todisplay.' : </div>';
        }
        echo '<div class="left student_photo" style="width: 80px;height:110px;margin:10px;">'.link_to(image_tag(url_for('student_photo',$student), array('style'=>'width:80px;','title'=>$student->getName())),url_for('student_big_photo',$student)).'</div>';
      }
      echo '<div class="clear"></div></div></div>';
    ?>
    <script type="text/javascript">
    $(document).ready(function()
    {
       $('.student_photo img[title]').qtip({
           position: {
               corner: {
                  tooltip: 'topMiddle',
                  target: 'bottomMiddle'
               }
            },
            style: {
               textAlign: 'center',
               tip: true,
               name: 'blue'
            }
       });
    });
    </script>
</div>