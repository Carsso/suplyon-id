<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <?php include_http_metas() ?>
  <?php include_metas() ?>
  <?php include_title() ?>
  <link rel="shortcut icon" href="/favicon.png"/>
  <?php include_stylesheets() ?>

  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
  <?php include_javascripts() ?>

  <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-21065019-1']);
    _gaq.push(['_setDomainName', 'suplyon.fr']);
    _gaq.push(['_trackPageview']);

    (function () {
      var ga = document.createElement('script');
      ga.type = 'text/javascript';
      ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(ga, s);
    })();

  </script>
</head>
<body>
<div id="messages" style="display: none;"></div>
<div id="header">
  <div id="header_content">
    <?php echo link_to(image_tag('suplyon.png'), '@homepage') ?>
  </div>
</div>

<div id="main">
      <?php echo $sf_content ?>
    <div id="footer">
        <p>Made by <a href="http://germain-carre.fr">Germain Carré</a> - Designed by <a href="http://maxime-steinhausser.fr">Maxime Steinhausser</a></p>
    </div>
  </div>

</div>


</body>
</html>
