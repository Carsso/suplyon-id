<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <?php include_http_metas() ?>
  <?php include_metas() ?>
  <?php include_title() ?>
  <link rel="shortcut icon" href="/favicon.png" />
  <?php include_stylesheets() ?>

  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
  <?php include_javascripts() ?>

  <style type="text/css">
    .ul_menu li {
      float:left;
      list-style: none outside none;
      margin:0 10px;
    }
  </style>
</head>
<body style="padding:0px;margin:0px;">
<div id="messages" style="display: none;"></div>
<div id="header">
  <div id="header_content">
    <?php echo link_to(image_tag('suplyon.png'),'@homepage') ?>
  </div>
</div>
<div id="connexion" class="right_bloc" style="float:none;width: 90%;margin: 10px auto;padding-bottom:5px ">
  <ul class="ul_menu">
    <li><a href="<?php echo url_for('member') ?>">Membres</a></li>
    <li><a href="<?php echo url_for('config') ?>">Config</a></li>
    <li><a href="/" target="_blank">Retour</a></li>
  </ul>
</div>
<div id="corps" style="float:none;width: 90%;margin: 10px auto; ">

  <div id="page_title"><h2>Administration</h2></div>
  <div style="margin:20px;">
  <?php echo $sf_content ?>
    <div style="clear:both"></div>
  </div>
</div>

<div id="footer" style="float:none;width: 90%;margin: 10px auto; ">
  <p style="margin:1em 0px">Made by <a href="http://germain-carre.fr">Germain Carré</a> - Designed by <a href="http://maxime-steinhausser.fr">Maxime Steinhausser</a></p>
</div>
</body>
</html>

