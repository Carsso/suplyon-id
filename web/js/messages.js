
function newNotification(type, message) {
    /*
        usage: newNotification(type, message);
        type (str): warning, success, error, info
        message (str): free txt
     */
    $('#messages').stop(true, true).html('<div class="msg_' + type + ' message"><h3>' + message + '</h3></div>');
    $('#messages').stop(true, true).show("slide", {
        direction: "down"
    }, 1000);
    $('#messages').click(function () {
        $('#messages').hide("slide", {
            direction: "down"
        }, 1000);
    });
    $('#messages').delay(7000).hide("slide", {
        direction: "down"
    }, 2000, function () {
        $('#messages').html('');
    });
}
